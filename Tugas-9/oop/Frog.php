<?php
    require_once("Animal.php");

    class Frog extends Animal{
        public $legs = 2;
        public $cold_blooded = "Yes";
        public function Jump($jalan){
            return "Jump : ". $jalan. "<br><br>";
        }
    }
?>