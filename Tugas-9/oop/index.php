<?php

    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");

    $sheep = new Animal("Shaun The Sheep");

        echo "Nama : " .$sheep->name. "<br>"; 
        echo "Legs : " .$sheep->legs. "<br>";
        echo "Cold Blooded ? : " .$sheep->cold_blooded. "<br>";
        echo "<br>";

    $frog = new Frog("Pangeran kodok");
        echo "Nama : " .$frog->name. "<br>"; 
        echo "Legs : " .$frog->legs. "<br>";
        echo "Cold Blooded ? : " .$frog->cold_blooded. "<br>"; 
        echo $frog->Jump("Hop Hop");
        echo "<br>";


    $ape = new Ape("Kera Sakti");

        echo "Nama : " .$ape->name. "<br>"; 
        echo "Legs : " .$ape->legs. "<br>";
        echo "Cold Blooded ? : " .$ape->cold_blooded. "<br>";
        echo "Yell : ".$ape->Yell();
        
?>