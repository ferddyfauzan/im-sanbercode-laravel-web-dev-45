<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('latihan1.register');
    }
  

    public function welcome(Request $register){
        $firs_name = $register['firs_name'];
        $last_name = $register['last_name'];
        $gender = $register['gender'];
        $biografi = $register['biografi'];
        $warganegara = $register['warganegara'];
        return view('latihan1.welcome', ['firs_name' => $firs_name,
                                         'last_name' => $last_name,
                                         'gender' => $gender,
                                         'biografi' => $biografi,
                                         'warganegara' => $warganegara
    ]);
    }
}
