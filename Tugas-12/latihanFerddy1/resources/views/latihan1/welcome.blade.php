@extends('layout.master')

@section('title')
    Register
@endsection

@section('content')
    <h1>SELAMAT DATANG!!!</h1>
    <h1>{{$firs_name}}{{$last_name}}</h1>

    <h2>Profil Singkat: </h2>
    <p>Nama : <b>{{$firs_name}} {{$last_name}}</b></p>
    <p>Jenis-Kelamin : <b>
        @if($gender === "1")
            Laki-Laki 
        @elseif($gender === "2")
            Perempuan
        @else
            Secret/Rahasia 
        @endif </b>
    </p>
    <p>Warganegara : <b>{{$warganegara}}</b></p>
    <p>Biografi singkat : <b>{{$biografi}}</b></p>

    <h2>Terimakasih Telah Bergabung di SanberBook. Social Media Kita Bersama!</h2>
@endsection