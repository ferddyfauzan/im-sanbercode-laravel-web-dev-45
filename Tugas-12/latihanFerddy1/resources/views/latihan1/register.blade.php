@extends('layout.master')

@section('title')
    Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="firs_name">Firs Name : </label><br>
        <input type="text" name="firs_name" id="firs_name"><br><br>
        <label for="last_name">Last Name : </label><br>
        <input type="text" name="last_name" id="last_name"><br><br>
        <label for="gender">Gender : </label><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>
        <label for="warganegara">Nationality: </label>
        <select name="warganegara" id="warganegara">
            <option value="indonesia">Indonesia</option>
            <option value="jepang">jepang</option>
            <option value="singapore">singapore</option>
            <option value="malaysia">malaysia</option>
            <option value="brunei">brunei</option>
            <option value="vietnam">malaysia</option>
        </select><br><br>
        <label for="bahasa">Language Spoken : </label> <br>
        <input type="checkbox" name="bahasa" id="indonesia">Indonesia <br>
        <input type="checkbox" name="bahasa" id="english">English <br>
        <input type="checkbox" name="bahasa" id="other">Other <br> <br>
        <label for="biografi">Bio : </label><br>
        <textarea name="biografi" id="biografi" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
@endsection