@extends('layout.master')

@section('title')
   Detail Profil Cast :  
@endsection

@section('content')

    <a href="/cast" class="btn btn-sm btn-primary">BACK</a>
    <h2>Name Cast : <strong>{{$cast->nama}}</strong></h2>
    <h2>Umur : <strong>{{$cast->umur}}</strong></h2>
    <p>Biography : <br> <br>{{$cast->bio}} </p>

@endsection