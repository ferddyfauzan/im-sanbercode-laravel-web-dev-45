@extends('layout.master')

@section('title')
    Edit Cast Profile:  
@endsection

@section('content')
    

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Name Cast :</label><br>
    <input type="text" name="nama" value="{{$cast->nama}}" class="@error('nama') is-invalid @enderror form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur : </label><br>
    <input type="number" name="umur" value="{{$cast->umur}}" min="10" max="100" class="@error('umur') is-invalid @enderror form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Biografi</label><br>
    <textarea name="bio" id="" cols="30" rows="10" class="@error('bio') is-invalid @enderror form-control">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="/cast" class="btn btn-primary">BACK</a>
</form>
@endsection