@extends('layout.master')

@section('title')
   Data Cast : 
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-sm btn-primary">Tambah Data Cast </a>
    <table class="table">
  <thead>
    <tr>
      <th scope="col">NO.</th>
      <th scope="col">Name Cast</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <th scope="row">{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a class="btn btn-info btn-primary btn-sm" href="/cast/{{$value->id}}">Profil Cast</a>
                        <a class="btn btn-info btn-warning btn-sm" href="/cast/{{$value->id}}/edit">Edit</a>
                        <input type="submit" value="delete" class="btn btn-info btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <p>No users</p>
        @endforelse
    
  </tbody>
</table>
@endsection