@extends('layout.master')

@section('title')
    Create Cast :  
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Name Cast :</label><br>
    <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur : </label><br>
    <input type="number" name="umur" min="10" max="100" class="@error('umur') is-invalid @enderror form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Biografi</label><br>
    <textarea name="bio" id="" cols="30" rows="10" class="@error('bio') is-invalid @enderror form-control"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection